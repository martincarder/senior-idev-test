module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
        js : {
            src : [
                'js/libs/handlebars-v2.0.0.js', 'js/libs/underscore.js', 'js/libs/jquery-1.11.1.min.js', 'js/libs/backbone-min.js', 'js/templates.js', 'scripts/jscrollpane/jquery.jscrollpane.min.js', 'js/data/data.js', 'js/model/product.js', 'js/view/porductSelect.js', 'js/view/productList.js', 'js/view/mobileDetails.js', 'js/app.js'
            ],
            dest : 'js/production/jscombined.js'
        }
    	},
        uglify: {
            js: {
                files: {
                    'js/production/fullapp.min.js': ['js/production/jscombined.js']
                }
            }
        },
        sass: {
          dev: {
            options: {
              style: 'expanded',
              compass: false
            },
            files: {
              'styles/styles.css': 'styles/sass/*.scss'
            }
          },  
          dist: {
            options: {
              style: 'compressed',
              compass: false
            },
            files: {
              'styles/styles.css': 'styles/sass/*.scss'
            }
          }
        },
        /**
         * Watch
         */
        watch: {
          sass: {
            files: 'styles/sass/*.scss',
            tasks: ['sass:dev']
          }
        }
   

    });
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default', ['sass:dist','concat:js','uglify:js']);
};