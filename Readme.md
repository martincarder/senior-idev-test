#  Martin Carder Syzygy Technical Test‏ 

I've made use of jQuery, underscore, backbone and handlebars. For CSS pre-processing I've used SASS and Grunt is my task runner of choice. The code allows for easy expansion of the number of selectors (however the CSS would need to be modified).

A full functioning version of the test is available at [http://www.martincarder.com/Syzygy/](http://www.martincarder.com/Syzygy/) it has been tested in the following browsers:

##Windows XP/7/8##
 - ie6
 - ie7
 - ie8
 - ie9
 - ie10
 - ie11
 - Chrome 38
 - Firefox 33

##OSX Maverick##
 - Safari
 - Chrome 38
 - Firefox 33

##Android 4.4.4##
 - Chrome 38

##iOS 7.1/8.1 ##
 - Safari

##ie 6/7 Notes##

 - Accessibility - has not been tested in ie 6/7

##Additional notes##

Accessibility could be expanded further with use of ARIA live regions.


 
