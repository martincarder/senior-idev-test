var app;
var numberOfSelects = 3;
$(document).ready(function(){
	
	app = new AppRouter();

	//used for app wide events
	app.vent = _.extend({}, Backbone.Events);

	app.start();

});

var AppRouter = Backbone.Router.extend({
	start: function(){
		
		//create product collection
		this.data = new productsCollection(window.prodData);

		for(var i = 0; i<numberOfSelects; i++)
		{
			//create product select view and add to the dom
			this.products1 = new productSelectView({model:this.data, selectionID:i});
			this.products1.render();
			$('#wrapper #selects').append(this.products1.el);

			//create product details view and add to the dom
			this.prolist1 = new productListView({model:this.data, selectionID:i});
			$('#wrapper #details').append(this.prolist1.el);

		}
		//create mobile mobile details view
		//only visable on small screens
		this.mobileDetailsView = new mobileDetails({count:numberOfSelects});
		this.mobileDetailsView.render();
		$('#selects').after(this.mobileDetailsView.el);

	}

})