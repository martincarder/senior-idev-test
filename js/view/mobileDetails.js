//only visable on a small screen
window.mobileDetails = Backbone.View.extend({
	id:'mobileSelect',
	events:{
		"click a.left":"clickLeft",
		"click a.right":"clickRight"
	},
	initialize:function(options){
		this.numberOfSelects = options['count']-1;
		this.current = 0;

		//listen for select:changed event fired when a user choese an option form a select
		window.app.vent.on("select:change", this.setCurrent, this);
		
		//listen for mobile:name event fired by the active prodouct list so we can set the active product name
		window.app.vent.on("mobile:name", this.setProductName, this);
	},
	render:function(){
		
		var template = Handlebars.compile(window.mobileDetailsTemplate);
		var html = template();
		this.$el.append(html);

		this.textArea = this.$el.find('span');
	},
	clickLeft:function(ev){
		
		ev.preventDefault();
		this.current--;
		if(this.current < 0)
			this.current = this.numberOfSelects;

		this.update(this.current);
	},
	clickRight:function(ev){
		
		ev.preventDefault();
		this.current++;
		if(this.current > this.numberOfSelects)
			this.current = 0;
		
		this.update(this.current);
	},
	update:function(num){

		//lett anything that is listen know what prodocut list should be displayed
		app.vent.trigger("product:change", {selectionID:this.current});
	},
	setCurrent:function(obj){
		//When the user selects an option we need to update the curent counter so we know which product description we are showing 
		this.current = obj['selectionID'];
	},
	setProductName:function(obj){
		//when a product fetaure list is active we sent the selecter name here
		this.textArea.empty().append(obj['name']);
	}
})