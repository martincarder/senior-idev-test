window.productListView = Backbone.View.extend({

	className:'productList',
	initialize:function(options){
		//add event listener for selector update events
		window.app.vent.on("select:change", this.render, this);
		window.app.vent.on("product:change", this.checkToActivate, this);
		this.template = Handlebars.compile(window.productTemplate);
		//set selection id so we know which selector to update for
		this.selectionID = options['selectionID'];
		this.productName = "No Selection";
	},
	render:function(obj){
		//if the event has come from our paired selected then update the view with the new product
		if(obj['selectionID'] == this.selectionID)
		{
			var data = this.model.get(obj['productID']).toJSON();
			var html = this.template(data);
			this.productName = data['name'];
			this.$el.empty().append(html);
		}

		this.checkToActivate(obj);
	},
	checkToActivate:function(obj){
		//check if we are the active product list
		if(obj['selectionID'] == this.selectionID)
		{
			this.$el.addClass('display');

			//tell anyone listening the product name
			app.vent.trigger("mobile:name", {name:this.productName});
		}
		else
			this.$el.removeClass('display');
	}
});