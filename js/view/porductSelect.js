window.productSelectView = Backbone.View.extend({
	className:'productSelect',
	events:{
		"change select":"optionSelected",
		"focus select":"selectFocus",
		"blur select":"selectBlur"

	},
	initialize:function(options){
		
		this.model.on('change', this.changed, this);
		this.selectionID = options['selectionID'];
		this.currentSelection = -1;

		window.app.vent.on("select:change", this.optionLock, this);

		window.app.vent.on("product:change", this.checkHighlight, this);
	},
	render:function(){
		
		//generate template
		var template = Handlebars.compile(window.selectTemplate);
		var html = template(this.model.toJSON());
		
		//attache template to view
		this.setElement(html);

		this.$el.find('select').attr('tabindex', (this.selectionID+1));

	},
	optionSelected:function(ev){

		var curentOp = this.$el.find('select option[value="'+ev.target.value+'"]');
		
		if(oldIE && curentOp.prop("disabled") == true){
			//for ie 6 / 7 make sure the disable prop works on select
			this.$el.find('select option[value="'+this.currentSelection+'"]').prop('selected', true);

		}else
		{
		
			//the user has made a selection trigger an event and send the product ID 
			//, previously selected id and this selectors ID
			app.vent.trigger("select:change", {productID:ev.target.value, prevSelection:this.currentSelection, selectionID:this.selectionID});
			this.currentSelection = ev.target.value;

		}


	},
	optionLock:function(obj){
		
		this.$el.find("select option[value='"+ obj['prevSelection'] + "']").attr('disabled', false );

		if(obj['selectionID'] != this.selectionID)
			this.$el.find("select option[value='"+ obj['productID'] + "']").attr('disabled', true ); 
		
		//for ie 6 / 7 make sure the disable prop works on select
		if(oldIE){
			this.$el.find("select option").css("color", "black");
			this.$el.find("select option[disabled]").css("color", "graytext");
		}
		this.checkHighlight(obj);

	},
	checkHighlight:function(obj){
		if(obj['selectionID'] == this.selectionID)
		{
			this.$el.addClass('active');
			this.$el.removeClass('hasFocus');
		}else
		{
			this.$el.removeClass('active');
		}

	},
	selectFocus:function(){
		this.$el.addClass('hasFocus');
	},
	selectBlur:function(){
		this.$el.removeClass('hasFocus');
	}

		
})