window.selectTemplate = '\
<div class="selector">\
<span class="selectDefault">Select</span>\
<select>\
	<option value="-1">Select</option>\
	{{#each @root}}\
		<option value="{{ id }}">{{ name }}</option>\
	{{/each}}\
</select>\
</div>\
';

window.productTemplate = '\
<ul>\
	<li>Price £{{price}}</li>\
	<li>OS {{os}}</li>\
	<li>Dimensions (mm) {{dimensions}}</li>\
	<li>Weight (kg) {{weight}}</li>\
	<li>Transport weight (kg) {{tranWeight}}</li>\
	<li>Processor {{processor}}</li>\
	<li>Processor speed (GHz) {{processorSpeed}}</li>\
	<li>Ram {{ram}}</li>\
	<li>Type of storage {{storage}}</li>\
	<li>Storage capacity (GB) {{storageCapacity}}</li>\
</ul>\
';

window.mobileDetailsTemplate = '\
<a href="#" class="left" tabindex="4"><span>Show Previous</span><img src="images/arrow-left.gif" alt="Show Next" /></a>\
<span>No Selection</span>\
<a href="#" class="right" tabindex="5"><span>Show Previous</span><img src="images/arrow-right.gif" alt="Show Next" /></a>\
';